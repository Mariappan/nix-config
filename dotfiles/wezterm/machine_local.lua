--
-- This file is ephemeral and will be overwritten on nixos build
--
-- If you want to persist changes to this file, copy this file to
-- `~/.config/wezterm/machine_local.lua` and edit
--

local module = {}

local wezterm = require("wezterm")

function module.apply(config)
  -- Add your local customizations here
end

return module
