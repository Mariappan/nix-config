import json
import urllib.request
from datetime import datetime

WEATHER_CODES = {
    '113': '☀️',
    '116': '⛅️',
    '119': '☁️',
    '122': '☁️',
    '143': '🌫',
    '176': '🌦',
    '179': '🌧',
    '182': '🌧',
    '185': '🌧',
    '200': '⛈',
    '227': '🌨',
    '230': '❄️',
    '248': '🌫',
    '260': '🌫',
    '263': '🌦',
    '266': '🌦',
    '281': '🌧',
    '284': '🌧',
    '293': '🌦',
    '296': '🌦',
    '299': '🌧',
    '302': '🌧',
    '305': '🌧',
    '308': '🌧',
    '311': '🌧',
    '314': '🌧',
    '317': '🌧',
    '320': '🌨',
    '323': '🌨',
    '326': '🌨',
    '329': '❄️',
    '332': '❄️',
    '335': '❄️',
    '338': '❄️',
    '350': '🌧',
    '353': '🌦',
    '356': '🌧',
    '359': '🌧',
    '362': '🌧',
    '365': '🌧',
    '368': '🌨',
    '371': '❄️',
    '374': '🌧',
    '377': '🌧',
    '386': '⛈',
    '389': '🌩',
    '392': '⛈',
    '395': '❄️'
}

data = {}


def format_time(time):
    return time.replace("00", "").zfill(2)


def format_temp(temp):
    return (hour['FeelsLikeC']+"°").ljust(3)


def format_chances(hour):
    chances = {
        "chanceoffog": "Fog",
        "chanceoffrost": "Frost",
        "chanceofovercast": "Overcast",
        "chanceofrain": "Rain",
        "chanceofsnow": "Snow",
        "chanceofsunshine": "Sunshine",
        "chanceofthunder": "Thunder",
        "chanceofwindy": "Wind"
    }

    conditions = []
    for event in chances.keys():
        if int(hour[event]) > 0:
            conditions.append(chances[event]+" "+hour[event]+"%")
    return ", ".join(conditions)


try:
    res = urllib.request.urlopen('https://wttr.in/Singapore?format=j1').read()
    weather = json.loads(res.decode("utf-8"))
except Exception:
    data["text"] = "菱"
    print(json.dumps(data))
    exit(0)

w = weather['current_condition'][0]

data['text'] = WEATHER_CODES[w['weatherCode']] + " " + w['FeelsLikeC'] + "°"

data['tooltip'] = f"<b>{w['weatherDesc'][0]['value']} {w['temp_C']}°</b>\n"
data['tooltip'] += f"Feels like: {w['FeelsLikeC']}°\n"
data['tooltip'] += f"Wind: {w['windspeedKmph']}Km/h\n"
data['tooltip'] += f"Humidity: {w['humidity']}%\n"
for i, day in enumerate(weather['weather']):
    data['tooltip'] += "\n<b>"
    if i == 0:
        data['tooltip'] += "Today, "
    if i == 1:
        data['tooltip'] += "Tomorrow, "
    data['tooltip'] += f"{day['date']}</b>\n"
    data['tooltip'] += f"⬆️ {day['maxtempC']}° ⬇️ {day['mintempC']}° "
    data['tooltip'] += f" {day['astronomy'][0]['sunrise']} "
    data['tooltip'] += f" {day['astronomy'][0]['sunset']}\n"

    for hour in day['hourly']:
        if i == 0:
            if int(format_time(hour['time'])) < datetime.now().hour-2:
                continue
        data['tooltip'] += f"{format_time(hour['time'])} "
        data['tooltip'] += f"{WEATHER_CODES[hour['weatherCode']]} "
        data['tooltip'] += f"{format_temp(hour['FeelsLikeC'])} "
        data['tooltip'] += f"{hour['weatherDesc'][0]['value']}, "
        data['tooltip'] += f"{format_chances(hour)}\n"


print(json.dumps(data))
