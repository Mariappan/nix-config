local M = {
  "marko-cerovac/material.nvim",
  lazy = true,
  init = function()
    vim.g.material_style = "darker"
  end
}

return M
